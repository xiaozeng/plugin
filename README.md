# Loon插件
_**自用 Yuheng VIP 资源以及收藏的一些 VIP 破解资源自用**_

##  1.系统增强

<details><summary>展开</summary>

| App | 地址 | 备注 |
| ------ | ------ | ------ |
| 脚本转换工具 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Script-Hub.plugin) | |
| 订阅管理工具 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Sub-Store.plugin) | |
| 数据管理工具 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/boxjs.plugin) | |
| Disney+评分 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/DisneyRating.plugin) | |
| Spotify歌词增强 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Spotify_lyrics_translation.plugin) | |
| Google搜索重定向 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Google.plugin) | |
| 京东比价 | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/JD_Price.plugin)~~ | 已失效 |
| 微信解除链接限制 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/UnblockURLinWeChat.plugin) | |
| Loon插件仓库| [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/LoonGallery.plugin) | |
| 网络延迟测试| [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Net_Speed.plugin) | |
| 节点检测工具 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Nodetool.plugin) | |
| 网络信息 𝕏 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/net-lsp-x.plugin) | |

</details>


##  2.解锁VIP

<details><summary>展开</summary>

| App | 地址 | 备注 |
| ------ | ------ | ------ |
| 酷我音乐 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Kuwo.plugin) | |
| 芒果TV | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/MGTV.plugin) | |
| 订阅通 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Subscriptions.plugin) | |
| 网易云音乐 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/NeteaseCloudMusic.plugin) | |
| Remini | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Remini.plugin) | |
| 爱企查 | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/aiqicha.plugin)~~ | 已失效 |
| bilibili | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/bilibili.plugin)~~ | 已失效 |
| 财新 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/caixin.plugin) | |
| 剪映 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/jianying.plugin) | |
| 迅雷 | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/thunder.plugin)~~ | 已失效 |
| 题库合集 | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/tiku.plugin)~~ | 已失效 |
| 喜马拉雅 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/xmly.plugin) | |

</details>


##  3.去广告

<details><summary>展开</summary>

| App | 地址 | 备注 |
| ------ | ------ | ------ |
| 高德地图 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Amap_ads.plugin) | |
| 微信公众号 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Weixin_remove_ads.plugin) | |
| 淘宝 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Taobao_remove_ads.plugin) | |
| 酷我音乐 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/kuwo_ads.plugin) | |
| 123网盘 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/123pan_ads.plugin) | |
| 百度网盘 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Baiducloud.plugin) | |

</details>

##  4.签到


<details><summary>展开</summary>

| App | 地址 | 备注 |
| ------ | ------ | ------ |
| 今日油价 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/oil_price.plugin) | |
| 今日汇率 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/rates.plugin) | |
| 永旺小程序 | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Aeon.plugin)~~ | 已失效 |
| 阿里云盘 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/AliDrive.plugin) | |
| 腾讯视频 | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/Tenvideo.plugin)~~ 已失效 |
| 阿里云盘 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/aliYunPan.plugin) | |
| 百度文库 | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/bdwk.plugin)~~ | 已失效 |
| 霸王茶姬 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/bwcj.plugin) | |
| 机场签到 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/checkincookie_env.plugin) | |
| 携程旅行 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/ctrip.plugin) | |
| i茅台 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/imaotai.plugin) | |
| 金多多商城 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/jinduod.plugin) | |
| 捷停车 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/jparking.plugin) | |
| 茅台小程序 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/maotai.plugin) | |
| 蜜雪冰城 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/mxbc.plugin) | |
| 奈雪的茶 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/naixue.plugin) | |
| 柠季 | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/ningji.plugin)~~ | 已失效 |
| 奶茶多合一 | [脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/teaMilk.plugin) | |
| 微信支付有优惠 | ~~[脚本地址](https://gitlab.com/xiaozeng/plugin/-/raw/main/wechat_pay_coupon.plugin)~~ | 已失效 |

</details>
